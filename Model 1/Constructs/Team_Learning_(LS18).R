l_tm_lrn <- dat %>% select(ID,Team,Employee,LT2P7Q29:LT2P7Q35)

l_tm_lrn[l_tm_lrn$Team==19,4:10] = rep(1,7)

l_tm_lrn$l_tm_l = rowMeans(l_tm_lrn[,4:10],na.rm = TRUE)

l_tm_lrn <- l_tm_lrn %>% select(ID,Team,Employee,l_tm_l)