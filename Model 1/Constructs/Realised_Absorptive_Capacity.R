rac <- dat %>% select(ID,Team,Employee,T2P1Q12:T2P1Q23)
rac$rac <- rowMeans(rac[,4:15],na.rm=TRUE)
rac <- rac %>% select(ID,Team,Employee,rac)