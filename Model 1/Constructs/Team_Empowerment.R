tm_empowr <- dat %>% select(ID,Team,Employee,T2P4Q19:T2P4Q30)
tm_empowr$tm_empowr <- rowMeans(tm_empowr[,4:15],na.rm=TRUE)
tm_empowr <- tm_empowr %>% select(ID,Team,Employee,tm_empowr)