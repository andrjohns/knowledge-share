##### Model 1 #####

## Libraries ##
library(readxl)
library(dplyr)
library(stringr)
library(Amelia)
library(MplusAutomation)

## Base Data ##
dat <- read_xlsx("Database_Merge_April2018.xlsx") %>%
             mutate(Team = str_sub(ZYID1,-4),
                    Employee = str_sub(Team,-1),
                    Team = factor(str_sub(Team,-4,-2),labels=seq.int(1,99,1)),
                    ID = seq.int(1,length(Employee),1))

## Constructs ##
source("Constructs/KOHR.R")
source("Constructs/Team_Empowerment.R")
source("Constructs/Team_Knowledge_Share_Climate.R")
source("Constructs/Psych_Safety.R")
source("Constructs/Employee_Performance_(LE).R")
source("Constructs/Team_Creativity_(LS12).R")
source("Constructs/Team_Performance_(LS13).R")
source("Constructs/Knowledge_Utilisation_(LS17).R")
source("Constructs/Team_Learning_(LS18).R")

## Analysis Dataset ##
a1 <- left_join(kohr,tm_empowr) %>% left_join(tm_kn_clm) %>%
                                    left_join(sps_sft) %>%
                                    left_join(emp_perf) %>%
                                    left_join(l_team_crea) %>%
                                    left_join(l_team_perf) %>%
                                    left_join(l_kn_util) %>%
                                    left_join(l_tm_lrn)

a1[is.na(a1)] <- -99

write.table(a1,file="M1.csv",row.names = FALSE,col.names=FALSE,quote = FALSE,sep=",")

## Cleanup ##
rm(list = ls()[ls() != "a1"])

## Create & Run Analyses ##
createModels("Model1_Template.txt")
createModels("Model1_Template_PS_Removed.txt")
runModels(paste0(getwd(),"/Mplus/NewOutcomes"),recursive=TRUE)

## Extract Summaries (Psych Safety Included)##
ps_mods <- readModels(paste0(getwd(),"/Mplus/NewOutcomes/PsychSafetyIncluded"),recursive = TRUE)
ps_summs <- SummaryTable(ps_mods,keepCols = c("Title","Observations","BIC","ChiSqM_Value","ChiSqM_DF",
                                        "ChiSqM_PValue","CFI","TLI","RMSEA_Estimate","RMSEA_90CI_LB",
                                        "RMSEA_90CI_UB","SRMR"))

## Extract Summaries (Psych Safety Removed)##
ps_r_mods <- readModels(paste0(getwd(),"/Mplus/NewOutcomes/PsychSafetyRemoved"),recursive = TRUE)
ps_r_summs <- SummaryTable(ps_r_mods,keepCols = c("Title","Observations","BIC","ChiSqM_Value","ChiSqM_DF",
                                        "ChiSqM_PValue","CFI","TLI","RMSEA_Estimate","RMSEA_90CI_LB",
                                        "RMSEA_90CI_UB","SRMR"))

## Run Final Model (Residual Covariance) ##
#runModels(paste0(getwd(),"/Mplus/Model_1_Final.inp"))
#rc <- readModels(paste0(getwd(),"/Mplus/Model_1_Final.out"))
#fin <- SummaryTable(rc,keepCols = c("Title","Observations","BIC","ChiSqM_Value","ChiSqM_DF",
#                                        "ChiSqM_PValue","CFI","TLI","RMSEA_Estimate","RMSEA_90CI_LB",
#                                        "RMSEA_90CI_UB","SRMR"))